#!/usr/bin/python3
# Change automatically the data sets and some parameters in a grace file

import numpy as np
import argparse
from lib.tuttilib import sequify
import sys

parser = argparse.ArgumentParser()
parser.add_argument("gname", type=str, help="xmgrace filename")
parser.add_argument("fnames", type=str, help="Data filenames", nargs="+")
parser.add_argument("-c", "--cols", type=str, default=None, help="Column indexes for each data files (1,2-4_3,4_...)")
parser.add_argument("-s", "--seq", type=str, default=None, help="Sequence of set indices")
parser.add_argument("-r", "--rep", type=str, default=None, help="Replace field 1 by 2, 3 by 4, etc. (f1,f2_f3,f4)")
parser.add_argument("--rval", type=str, default=None, help="Replace field 1 by value from sequences (f1,val1_f2,val2). val1, val2,... can be min or max.")
parser.add_argument("-a", "--ascale", action='store_true', help="Autoscale")
parser.add_argument("-x", "--xfac", type=str, default="1", help="Multiplicative factor for x (fx1_fx2_...)")
parser.add_argument("-f", "--yfac", type=str, default="1", help="Multiplicative factor for y (fy1_fy2_...)")
parser.add_argument("-o", "--oname", type=str, default=None, help="Output filename")
parser.add_argument("--ntx", type=int, default=None, help="Number of ticks, X axis")
parser.add_argument("--nty", type=int, default=None, help="Number of ticks, Y axis")
parser.add_argument("--nmax", type=str, default=None, help="Normalize the maxima to the sequence of numbers")
parser.add_argument("--navg", type=str, default=None, help="Normalize the average to the sequence of numbers")
args = parser.parse_args()

nf = len(args.fnames)

if args.cols == None:
    cols = [0]
else:
    cols = sequify(args.cols)

if nf == 1:
    cols = [cols]
else:
    if len(cols) == 1:
        cols = [[cols[0]] for i in range(nf)]

if len(cols) != nf:
    print("Error! Number of column indexes do not match number of files!")
    print(cols)
    sys.exit()


if args.seq == None:
    iseq = None
else:
    iseq = sequify(args.seq)

    if sum([len(i) for i in cols]) != len(iseq):
        print("Error! Inconsistent number of cols and iseq!")
        sys.exit()

if args.rep == None:
    rflds = [["", ""]]
else:
    rflds = [i.split(",") for i in args.rep.split("_")]

nrf = len(rflds)
for i in range(nrf):
    if len(rflds[i]) != 2:
        print("Error! Number of fields must be equal to 2!")
        sys.exit()

if args.rval == None:
    rvals = [["", ""]]
else:
    rvals = [i.split(",") for i in args.rval.split("_")]
    rvdic = {"min": 0, "max": 1}

nrv = len(rvals)
for i in range(nrv):
    if len(rvals[i]) != 2:
        print("Error! Number of vals must be equal to 2!")
        sys.exit()

norm = [None for i in range(nf)]
if args.nmax != None:
    if args.navg != None:
        print("Error! Cannot use nmax and navg options at the same time!")
        sys.exit()
    nop = "max"
    norm = sequify(args.nmax, nseq=nf)

if args.navg != None:
    nop = "mean"
    norm = sequify(args.navg, nseq=nf)

if args.oname == None:
    oname = ".".join(args.gname.split(".")[:-1])+"_r.agr"
else:
    oname = args.oname

xfac = sequify(args.xfac,ntot=nf)
yfac = sequify(args.yfac,ntot=nf)

nta = [args.ntx, args.nty]

dat = []
tmm = [[1e15, -1e15] for j in range(2)]
cmm = [[1e15, -1e15] for j in range(2)]

for l in range(nf):
    dat.append(np.loadtxt(args.fnames[l]))
    
    dat[l][:,0] = dat[l][:,0]*xfac[l]
    dat[l][:,1:] = dat[l][:,1:]*yfac[l]
    
    nr, nc = dat[-1].shape
    # Transform zeros as sequence of index from 1-nc-1
    for i in range(len(cols[l])):
        if cols[l][i] == 0:
            del cols[l][i]
            for j in range(nc-1,0,-1):
                cols[l].insert(i,j)
    
    #print(fname, cols)
    
    if norm[l] != 0:
        if norm[l] != None:
            for j in range(1, nc):
                dat[l][:,j] = dat[l][:,j]*norm[l]/getattr(dat[l][:,j], nop)()
        
        # We store the min/max of the x coordinates
        tmm[0] = [np.min(dat[l][:,0]), np.max(dat[l][:,0])]
        
        for c in cols[l]:
            # We store the min/max of the set of y coordinates
            tmp = np.min(dat[l][:,c])
            if tmm[1][0] > tmp:
                tmm[1][0] = tmp
            tmp = np.max(dat[l][:,c])
            if tmm[1][1] < tmp:
                tmm[1][1] = tmp
            #print(tmm, cmm)
        for j in range(2):
            for n in range(2):
                if (2*n-1)*(tmm[j][n]-cmm[j][n]) > 0:
                    cmm[j][n] = tmm[j][n]

for l in range(nf):
    if norm[l] == 0:
        for j in range(1, len(dat[l][0])):
            dat[l][:,j] = dat[l][:,j]*cmm[1][1]/getattr(dat[l][:,j], nop)()

if not args.rval is None:        
    for j in range(nrv):
        rvals[j][1] = "{:10.2e}".format(tmm[1][rvdic[rvals[j][1]]]).replace(" ", "")
#print(cmm)

prtline = True
k, l, c = -1, 0, -1
token1 = "@target G0."
token2 = "&"
token3 = "@    world "
token4 = ["xaxis", "yaxis"]
f1 = open(args.gname, encoding='utf-8',errors='ignore')
f2 = open(oname, "w")
for i, line in enumerate(f1):
    if token2 in line:
        prtline = True
    if args.ascale:
        if token3 in line:
            line = token3+",".join(map(str,[cmm[0][0],cmm[1][0],cmm[0][1],cmm[1][1]]))
        for j in range(2):
            if "@    "+token4[j]+"  tick on" in line and nta[j] != None:
                print(line.strip(), file=f2)
                f1.readline()
                stp = (cmm[j][1]-cmm[j][0])/nta[j]
                line = "@    "+token4[j]+"  tick major "+"{:.1e}".format(stp)
    if prtline:
        line = line.strip()
        for j in range(nrf):
            line = line.replace(rflds[j][0],rflds[j][1])
        for j in range(nrv):
            line = line.replace(rvals[j][0],rvals[j][1])
        print(line, file=f2)
    if token1 in line:
        k += 1
        if iseq == None or k in iseq:
            prtline = False
            print(f1.readline().strip(), file=f2)
            
            c += 1
            if c >= len(cols[l]):
                l +=1
                c = 0
            
            if l >= nf or c >= len(cols[l]):
                print("Error! To less columns to replace the sets!")
                sys.exit()
            
            print("Inserting col {} from {} to set {}".format(cols[l][c], args.fnames[l], k))
            
            for j in range(len(dat[l])):
                print(dat[l][j,0], dat[l][j,cols[l][c]], file=f2)

if l != nf-1 or c != len(cols[-1])-1:
    print("Warning! All columns have not been inserted!")
