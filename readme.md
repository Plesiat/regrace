# Readme

## About regrace

**regrace** is a Python code to replace the data sets contained in *.agr files (from Grace software) using one or several ASCII files containing numerical data. Mathematical operations on the replacing data can also be performed as well as renormalization and autoscaling.

## Installation

Requirements:
1. Python 3.0 and higher versions
2. Numpy

## Licence

This code is under GNU General Public License (GPL) version 3
